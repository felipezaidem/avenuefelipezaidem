﻿using AvenueCodeTestFelipeZaidem.Shared;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;
using System.Linq;
using OpenQA.Selenium;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AvenueCodeTestFelipeZaidem
{
    [Binding]
    public class FeatureAddTaskButtonSteps
    {
        ChromeDriver _browser;
        AutomatedTestHelper _automatedTestHelper;

        // This is a default text to be used in the tests
        private string testText = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.";

        /// <summary>
        /// It will store the last messaged used in the tests
        /// </summary>
        private string lastMessageAdded = string.Empty;

        [BeforeScenario]
        public void CreateWebDriver()
        {
            _browser = new ChromeDriver();
            _automatedTestHelper = new AutomatedTestHelper();

            // Opens the app and authenticates
            _automatedTestHelper.OpenApp(_browser);
            _automatedTestHelper.Authenticate(_browser);

            // Access the TODO list page
            _browser.FindElementsByLinkText("My Tasks").First().Click();
        }

        [AfterScenario]
        public void CloseWebDriver()
        {
            _browser.Close();
            _browser.Dispose();
        }

        [Given(@"I have fullfiled the textbox")]
        public void GivenIHaveFullfiled()
        {
            _browser.FindElementById("new_task").SendKeys("Name of task");
        }
        
        [Given(@"I have the empty field")]
        public void GivenIHaveTheEmptyField()
        {
            // Nothing to do
        }
        
        [Given(@"I have fill the field (.*) caracter")]
        public void GivenIHaveFillTheFieldCaracter(int p0)
        {
            lastMessageAdded = testText.Substring(0, p0);
            _browser.FindElementById("new_task").SendKeys(lastMessageAdded);
        }
        
        [Given(@"I have the fulfilled")]
        public void GivenIHaveTheFulfilled()
        {
            _browser.FindElementById("new_task").SendKeys("Name of task");
        }
        
        [When(@"I hit enter button")]
        public void WhenIHitEnterButton()
        {
            _browser.FindElementById("new_task").SendKeys(Keys.Enter);
        }
        
        [When(@"I click on the add button")]
        public void WhenIClickOnTheAddButton()
        {
            _browser.FindElementByClassName("input-group-addon").Click();
        }
        
        [Then(@"it should add the task")]
        public void ThenItShouldAddTheTask()
        {
            Assert.AreEqual(true, _browser.FindElementByXPath("//a[contains(text(),\"Name of task\")]").Displayed);
        }
        
        [Then(@"it should display a validation message")]
        public void ThenItShouldDisplayAValidationMessage()
        {
            // The item should not be added
            Assert.AreEqual(false, _browser.FindElementByXPath(string.Format("//a[contains(text(),\"{0}\")]", lastMessageAdded)).Displayed);
        }
        
        [Then(@"it should save the task")]
        public void ThenItShouldSaveTheTask()
        {
            Assert.AreEqual(true, _browser.FindElementByXPath("//a[contains(text(),\"Name of task\")]").Displayed);
        }
    }
}
