﻿using AvenueCodeTestFelipeZaidem.Shared;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AvenueCodeTestFelipeZaidem
{
    [Binding]
    public class FeatureVerifyMessageTopSteps
    {
        ChromeDriver _browser;
        AutomatedTestHelper _automatedTestHelper;

        [BeforeScenario]
        public void CreateWebDriver()
        {
            _browser = new ChromeDriver();
            _automatedTestHelper = new AutomatedTestHelper();
        }

        [AfterScenario]
        public void CloseWebDriver()
        {
            _browser.Close();
            _browser.Dispose();
        }

        [Given(@"I have clicked on My Task link")]
        public void GivenIHaveClickedOnMyTaskLink()
        {
            // Opens the app and authenticates
            _automatedTestHelper.OpenApp(_browser);
            _automatedTestHelper.Authenticate(_browser);

            // Clicks on the My Tasks link
            var linkMyTasksElement = _browser.FindElementsByLinkText("My Tasks").First();
            linkMyTasksElement.Click();
        }
        
        [When(@"I have a accessed ToDo list page")]
        public void WhenIHaveAAccessedToDoListPage()
        {
            // Nothing to do
        }
        
        [Then(@"Displayed a message ‘Hey John, this is your todo list for today'")]
        public void ThenDisplayedAMessageHeyJohnThisIsYourTodoListForToday()
        {
            Assert.AreEqual(true, _browser.FindElementByXPath(string.Format("//h1[contains(text(),\"Hey {0}, this is your todo list for today:\")]", _automatedTestHelper.Email)).Displayed);
        }
    }
}
