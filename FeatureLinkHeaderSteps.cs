﻿using AvenueCodeTestFelipeZaidem.Shared;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AvenueCodeTestFelipeZaidem
{
    [Binding]
    public class FeatureLinkHeaderSteps
    {
        ChromeDriver _browser;
        AutomatedTestHelper _automatedTestHelper;

        [BeforeScenario]
        public void CreateWebDriver()
        {   
            _browser = new ChromeDriver();
            _automatedTestHelper = new AutomatedTestHelper();
        }

        [AfterScenario]
        public void CloseWebDriver()
        {
            _browser.Close();
            _browser.Dispose();
        }

        [Given(@"I have a link My Task")]
        public void GivenIHaveALinkMyTask()
        {
            _automatedTestHelper.OpenApp(_browser);
            _automatedTestHelper.Authenticate(_browser);
        }
        
        [When(@"I click on the link")]
        public void WhenIClickOnTheLink()
        {
            var linkMyTasksElement  = _browser.FindElementsByLinkText("My Tasks").First();
            linkMyTasksElement.Click();
        }
        
        [Then(@"I should be redirected to the board My task")]
        public void ThenIShouldBeRedrectedToTheBoardMyTask()
        {
            Assert.AreEqual(true, _browser.FindElement(By.Id("new_task")).Displayed);
        }
    }
}
