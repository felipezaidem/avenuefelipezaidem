﻿using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvenueCodeTestFelipeZaidem.Shared
{
   public class AutomatedTestHelper
    {
        /// <summary>
        /// The user name
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// The user password
        /// </summary>
        public string Password { get; set; }

        public string UserName { get; set; }

        public AutomatedTestHelper()
        {
            Email = "felipezaidem@gmail.com";
            Password = "avenue123123";
            UserName = "Felipe Dias Zaidem";
        }

        /// <summary>
        /// Opens the app page
        /// </summary>
        /// <param name="browser"></param>
        public void OpenApp(ChromeDriver browser)
        {
            browser.Navigate().GoToUrl("https://qa-test.avenuecode.com/");
        }

        /// <summary>
        /// Authenticates the user
        /// </summary>
        /// <param name="browser"></param>
        public void Authenticate(ChromeDriver browser)
        {
           var signInLinkElement = browser.FindElementsByLinkText("Sign In").First();
            signInLinkElement.Click();

            var emailTextBoxElement = browser.FindElementById("user_email");
            emailTextBoxElement.SendKeys(Email);

            var passwordTextBoxElement = browser.FindElementById("user_password");
            passwordTextBoxElement.SendKeys(Password);

            // mudar para xpath
            var buttonElement = browser.FindElementByClassName("btn-primary");
            buttonElement.Click(); 
        }
    }
}