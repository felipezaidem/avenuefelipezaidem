﻿Feature: FeatureAddTaskButton
	The user should be able to enter a new task by hitting enter or clicking on the add task button.
	The task should require at least three characters so the user can enter it.
	The task can’t have more than 250 characters.
	When added, the task should be appended on the list of created tasks


Scenario: Verify if it saves a new Task
	Given I have fullfiled the textbox
	When I hit enter button
	Then it should add the task

Scenario: Verify if the error message is displayed
	Given I have the empty field
	When I hit enter button
	Then it should display a validation message

Scenario: Verify the minimal field lenght alowed
	Given I have fill the field 2 caracter
	When I hit enter button
	Then it should display a validation message

Scenario: Verify if the add button
	Given I have the fulfilled
	When I click on the add button
	Then it should save the task

Scenario: Verify if the app accepts more than max field lenght alowed
	Given I have fill the field 251 caracter
	When I hit enter button
	Then it should display a validation message

Scenario: Verify the max field lenght alowed
	Given I have fill the field 250 caracter
	When I hit enter button
	Then it should save the task