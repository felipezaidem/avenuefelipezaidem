﻿Feature: FeatureVerifyMessageTop
	The user should see a message on the top part saying that list belongs to the logged user.
	eg.: If the logged user name is John, then the displayed message should be ‘Hey John, this is your todo list for today:’


Scenario: Verify if the message text is displayed
	Given I have clicked on My Task link
	When I have a accessed ToDo list page
	Then Displayed a message ‘Hey John, this is your todo list for today'