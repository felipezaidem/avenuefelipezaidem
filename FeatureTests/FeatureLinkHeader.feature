﻿Feature: FeatureLinkHeader
	The user should always see the ‘My Tasks’ link on the NavBar
	Clicking this link will redirect the user to a page with all the created tasks so far


Scenario: Valid link My Task text and if it is broken
	Given I have a link My Task
	When I click on the link 
	Then I should be redirected to the board My task